var mongoose = require('mongoose');

var offerSchema = mongoose.Schema({
  id: Number,
  idEncoded: String,
  url: String,
  title: String,
  subtitle: String,
  description: String,
  descriprionTitle: String,
  instructions: String,
  instructionsTitle: String,
  rewardName: String,
  rewardNamePlural: String,
  rewardAmount: String,
  callToActionVerb: String,
  imageSrc: String,
  created: {type: Date, default: Date.now},
});

// compile the model
var Offer = mongoose.model('Offer', offerSchema);