var mongoose = require('mongoose');
var express = require('express');
var swig = require('swig');

var models = require('./models');
var routes = require('./routes');
var middleware = require('./middleware');

mongoose.connect('mongodb://localhost/offerwall', function(err) {
  if (err) throw err;
  
  var app = express();
  
  // assign the swig engine to .html files
  app.engine('html', swig.renderFile);
  
  // set .html as the default extension 
  app.set('view engine', 'html');
  
  app.set('view cache', false);
  swig.setDefaults({ cache: false });
  
  middleware(app);
  routes(app);
  
  app.listen(3000, function() {
    console.log('Listening on http://localhost:3000');
  });
});