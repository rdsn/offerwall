var errors = require('./errors');
var offers = require('./offers');
var mongoose = require('mongoose');
var Offer = mongoose.model('Offer');

module.exports = function(app) {

  // home page
  app.get('/', function(req, res) {
    // pass query params to template
    res.render('index', {
      sid: 'u123123',
      tid: '123123',
      tidEncoded: 'iIOJkwemj',
      currencyName: 'Points',
      logoSrc: 'https://dev.trialpay.com:4856/store/194097/12417710.png?506077'
    });
  });
    
  // offers
  offers(app);
  
  // error handlers
  errors(app);
};