var mongoose = require('mongoose');
var Offer = mongoose.model('Offer');

var modelExample = {
  id: 7708,
  idEncoded: 'eYV0YYs',
  url: 'http://google.com',
  title: 'CMM YOUTUBE (7290)',
  subtitle: 'SOCIAL OVERLAY HEADLINE',
  description: 'Browse the world’s most magical movie library as a member of the Disney Movie Club. Pick 3 of',
  descriptionTitle: 'About',
  instructions: '<ul><li>DO NOT TOUCH</li><li><b>Please note:</b> If you cancel your order, your Points may be revoked.</li></ul>',
  instructionsTitle: 'To Earn 19,500 Points',
  rewardName: 'Point',
  rewardNamePlural: 'Points',
  rewardAmount: '9,770',
  callToActionVerb: 'Earn', // incentivize,
  imageSrc: 'https://dev.trialpay.com:4856/store/1685516/12945626.jpg?503811',
  //created: date goes here...
};

module.exports = function(app) {
  app.get('/offers', function(req, res, next) {
    var page =  req.query.page || 1;
    var per_page = req.query.per_page || 10;
    
    Offer.find().sort('created').skip((page - 1) * per_page).limit(per_page).exec(function (err, posts) {
      if (err) return next(err);
      
//      setTimeout(function() {
        res.json(posts);
//      }, 1000);
    });
  });
  
  app.get('/addOffers', function(req, res, next) {
    var offers = [];
  
    for (var i = 0; i < 30; i++) {
      offers.push({
        id: i,
        idEncoded: 'encoded' + i,
        url: 'http://yandex.ru' + i,
        title: 'Title' + i,
        subtitle: 'Subtitle' + i,
        description: 'Description' + i,
        descriptionTitle: 'About' + i,
        instructions: '<ul><li>DO NOT TOUCH</li><li><b>Please note:</b> If you cancel your order, your Points may be revoked.</li></ul>',
        instructionsTitle: 'To Earn 19,500 Points',
        rewardName: 'Point',
        rewardNamePlural: 'Points',
        rewardAmount: '9,770' + i,
        callToActionVerb: 'Earn', // incentivize,
        imageSrc: 'https://dev.trialpay.com:4856/store/1685516/12945626.jpg?503811'
      });
    }
  
    Offer.create(offers, function(err) {
      if (err) return next(err);
      
      res.send('Offers created!');
    });
  });
};