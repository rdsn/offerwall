var express = require('express');
var sass = require('node-sass');

module.exports = function(app) {
  app.use(express.logger('dev'));
  
  app.use(
    sass.middleware({
      src: __dirname + './../public',
      dest: __dirname + './../public',
      debug: true,
      outputStyle: 'compressed'
    })
  );
  
  app.use(express.static(__dirname + './../public'))
  
  // this is good enough for now but you'll
  // want to use connect-mongo or similar
  // for persistant sessions
  app.use(express.cookieParser());
  
  app.use(express.bodyParser());
};