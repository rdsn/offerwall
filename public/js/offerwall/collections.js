Offerwall.module('Collections', function(Collections, App) {
  Collections.Offers = Backbone.PageableCollection.extend({
    model: App.Models.Offer,
    url: '/offers',
    state: {
      pageSize: 6,
      totalRecords: 30 // todo: set this in the server reponse
    }
  });
});