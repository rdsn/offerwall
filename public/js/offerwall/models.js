Offerwall.module('Models', function(Models, App) {
  Models.Offer = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      id: '', // id may interfere with default model id?
      idEncoded: '',
      url: '',
      title: '',
      subtitle: '',
      buttonText: 'Earn something', //todo: get rid of this
      description: '',
      descriptionTitle: 'About',
      instructions: '',
      instructionsTitle: '',
      rewardName: 'Point',
      rewardNamePlural: 'Points',
      rewardAmount: '2',
      callToActionVerb: 'Earn',
      imageSrc: ''
    }
  });
});
//
//var offer = {
//  id: 7708,
//  idEncoded: 'eYV0YYs',
//  url: 'http://google.com',
//  title: 'CMM YOUTUBE (7290)',
//  subtitle: 'SOCIAL OVERLAY HEADLINE',
//  description: 'Browse the world’s most magical movie library as a member of the Disney Movie Club. Pick 3 of your favorites for $1.99 each -- with FREE shipping and processing on your initial order when you join. Choose from beloved classics and the newest hits. Satisfaction 100% guaranteed.',
//  descriptionTitle: 'About',
//  instructions: '<ul><li>DO NOT TOUCH</li><li><b>Please note:</b> If you cancel your order, your Points may be revoked.</li></ul>',
//  instructionsTitle: 'To Earn 19,500 Points',
//  rewardName: 'Point',
//  rewardNamePlural: 'Points',
//  rewardAmount: '9,770',
//  callToActionVerb: 'Earn', // incentivize,
//  imageSrc: 'https://dev.trialpay.com:4856/store/1685516/12945626.jpg?503811'
//};