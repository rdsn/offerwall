Offerwall.module('Layouts', function(Layouts, App) {
  Layouts.Header = Marionette.ItemView.extend({ // Layout?
    el: '#header',
    template: '#header-template',
    serializeData: function() {
      return {
        'logoSrc': this.options.logoSrc,
        'currencyName': this.options.currencyName
      }
    }
  });
  
  Layouts.ModalContainer = Marionette.Layout.extend({
    id: 'modal-container',
    template: '#modal-container-template',
    regions: {
      modal: '#modal'
    },
    
    events: {
      'click': 'closeModal'
    },
    
    modalOverlay: $('<div id="modal-overlay" />'),
    
    initialize: function(options) {
      if (options.customClassPostfix) {
        this.$el.addClass(this.id + '_' + options.customClassPostfix);
      }
    },
    
    closeModal: function(e) {
      var eTarget = $(e.target); 
      
      if (eTarget.is(this.regions.modal) || eTarget.closest(this.regions.modal).length) return;
      
      App.commands.execute('closeModal');
    },
    
    onRender: function() {
      $('body').prepend(this.modalOverlay);
    },
    
    onClose: function() {
      this.modalOverlay.remove();
    }
  });
});