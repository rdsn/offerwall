Offerwall.module('Main', function(Main, App, Backbone, Marionette, $, _) { //workflow???
  Main.Router = Marionette.AppRouter.extend({
    appRoutes: {
      '': 'index',
      'page/:number': 'showPage',
      'page/:number/offer/:id': 'showOfferDetails'
    }
  });
  
  Main.Controller = function(options) {
    this.options = options;
    
    new App.Layouts.Header(options).render();
    
    this.offers = new App.Collections.Offers();
    
    this.offers.on('pageSet', function(pageNum) {
      App.router.navigate("page/" + pageNum);
    });
  };
  
  _.extend(Main.Controller.prototype, {
    start: function(pageNum) {
      App.state.isStarted = true;
      
      this.showOffers();
      this.showPagination();
    },
    
    getPage: function(pageNum) {
      pageNum = parseInt(pageNum);
      
      return this.offers.getPage(pageNum, {reset: true}).done(_.bind(function() {
        if (!App.state.isStarted) { // anti-history back case
          this.start();  
        }
      }, this));
    },
    
    //getPageAndStart: function() {}, // ???
    
    index: function() {
      App.router.navigate("page/" + 1);
      this.getPage(1);
    },
    
    showPage: function(pageNum) {
      this.closeModal();
      
      this.getPage(pageNum);
    },
    
    showOffers: function() {
      App.main.show(new App.Views.Offers({
        collection: this.offers
      }));
    },
    
    showPagination: function() {
      new App.Views.PageNavigation({
        collection: this.offers
      }).render();
    },
    
    showOfferDetails: function(pageNum, offerIdEncoded) {
      var offerModel;
      
      this.getPage(pageNum).done(_.bind(function() {
        offerModel = this.offers.findWhere({idEncoded: offerIdEncoded});
        
        this.showOfferDetailsByModel(offerModel);
      }, this));
    },
    
    showOfferDetailsByModel: function(model) {
      this.openModal({customClassPostfix: 'offer-details'});
      
      App.ui.modalContainer.modal.show(new App.Views.OfferDetails({model: model}));
    },
    
    openModal: function(options) {
      App.ui.modalContainer = new App.Layouts.ModalContainer(options);
      
      $('body').addClass('modal-opened').prepend(App.ui.modalContainer.render().el);
    },
    
    closeModal: function() {
      if (!App.ui.modalContainer) return; // already closed
      
      $('body').removeClass('modal-opened');
      
      App.ui.modalContainer.close();
    }
  });
  
  App.addInitializer(function(options) {
    App.commands.setHandlers({
      'offerDetails:show': function(model) {
        App.router.navigate("page/" + controller.offers.state.currentPage + '/offer/' + model.get('idEncoded'));
        
        controller.showOfferDetailsByModel(model);
      },
      'offerDetails:close': function() {
        App.commands.execute('closeModal');
      },
      'closeModal': function() {
        App.router.navigate("page/" + controller.offers.state.currentPage);
        
        controller.closeModal();
      }
    });
    
    var controller = new Main.Controller(options);
    
    App.router = new Main.Router({
      controller: controller
    });
  });
});

// todo: page caching