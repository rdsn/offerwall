Offerwall.module('Views', function(Views, App) {
  Views.Offer = Marionette.ItemView.extend({
    template: '#item-template',
    className: 'offer',
    events: {
      'click': 'handleClick'
    },
    modelEvents: {
      'change': 'render'
    },
    
    handleClick: function(e) {
      this.showDetails();
    },
    
    showDetails: function() {
      App.commands.execute('offerDetails:show', this.model);
    },
    
    onRender: function() {
      //console.log('Offer rendered');
    }
  });
  
  Views.Offers = Marionette.CollectionView.extend({
    id: 'offers',
    itemView: Views.Offer,
    
    sectionHtml: $('#offers-section-template').html(),
    lastSection: null,
    
    onItemRemoved: function(itemView) {
      // ItemViews are removed by Marionette, item wrappers must be removed manually
      this.$('> :empty').remove();
    },
    
    appendHtml: function(collectionView, itemView, index) {
      var targetEl = collectionView[collectionView.isBuffering ? 'elBuffer' : 'el'];
      
      if (index % 2 == 0) {
        this.lastSection = $(this.sectionHtml).appendTo(targetEl);
      }
      
      this.lastSection.append(itemView.el);
    }
  });
  
  Views.OfferDetails = Marionette.ItemView.extend({
    template: '#offer-details-template',
    id: 'offer-details',
    events: {
      'click .button_back': 'closeDetails',
      'click .button_continue': 'handleOffer'
    },
    
    handleOffer: function() {
      alert('something')
    },
    
    closeDetails: function() {
      App.commands.execute('offerDetails:close');
    }
  });
  
  Views.PageNavigation = Marionette.ItemView.extend({
    el: '#page-navigation',
    template: _.template($('#page-navigation-template').html()),
    
    getPageOptions: {
      reset: true // reset allows to use DocumentFragment (improves performance)
    },
    
    ui: {
      'prev': '.page-navigation__prev',
      'next': '.page-navigation__next'
    },
    
    events: {
      'click @ui.prev': 'goPrev', 
      'click @ui.next': 'goNext' 
    },
    
    collectionEvents: {
      'reset': 'render'
    },
    
    render: function() {
      var state = this.collection.state;
      
      this.$el.html(this.template({
        firstPage: state.firstPage,
        currentPage: state.currentPage,
        totalPages: state.totalPages
      }));
      
      return this; // good practice
    },
    
    goPrev: function() {      
      this.collection.getPreviousPage(this.getPageOptions);
      
      this.triggerPageSet();
    },
    
    goNext: function() {
      this.collection.getNextPage(this.getPageOptions);
      
      this.triggerPageSet();
    },
    
    triggerPageSet: function() {
      this.collection.trigger('pageSet', this.collection.state.currentPage);
    }
  });
});