var Offerwall = new Marionette.Application({
  ui: {}, // cache ui elements
  state: {}
});

Offerwall.addRegions({
  header: '#header',
  main: '#main',
  footer: '#footer'
});

Offerwall.on('start', function() {
  console.log('App.start');
  
  Backbone.history.start();
});

// Todo: Header currency